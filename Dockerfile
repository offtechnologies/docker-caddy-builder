FROM golang:1.12.0-alpine

RUN apk add --no-cache git gcc musl-dev

COPY build.sh /usr/bin/build.sh

CMD ["/bin/sh", "/usr/bin/build.sh"]
