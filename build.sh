#!/bin/sh
VERSION=${VERSION:-"1.0.1"}
TELEMETRY=${ENABLE_TELEMETRY:-"false"}
IMPORT="github.com/caddyserver/caddy"

# get caddy source
git clone https://$IMPORT -b "v$VERSION" /go/src/$IMPORT \
    && cd /go/src/$IMPORT \
    && git checkout -b "v$VERSION"

# disable telemetry
run_file="/go/src/$IMPORT/caddy/caddymain/run.go"
if [ "$TELEMETRY" = "false" ]; then
    cat > "$run_file.disablestats.go" <<EOF
    package caddymain
    import "os"
    func init() {
        switch os.Getenv("ENABLE_TELEMETRY") {
        case "0", "false":
            EnableTelemetry = false
        case "1", "true":
            EnableTelemetry = true
        }
    }
EOF
fi

# build caddy
cd  /go/src/$IMPORT/caddy \
    && export GO111MODULE=on \
    && CGO_ENABLED=0 go build \
    && mkdir -p /install \
    && cp caddy /install \
    && /install/caddy -plugins \
    && /install/caddy -version
