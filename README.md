## docker-caddy-builder

[![pipeline status](https://gitlab.com/offtechnologies/docker-caddy-builder/badges/master/pipeline.svg)](https://gitlab.com/offtechnologies/docker-caddy-builder/commits/master)

[offtechurl]: https://gitlab.com/offtechnologies

[![offtechnologies](https://gitlab.com/offtechnologies/logos/raw/master/logo100.png)][offtechurl]

docker-caddy-builder

> **Important Note**: Use on production servers at your own risk!

## How to Use

```bash
docker run --rm -v $(pwd):/install registry.gitlab.com/offtechnologies/docker-caddy-builder:latest
```
## Environment Variables

* `VERSION` - Caddy version. Default `1.0.1`
* `ENABLE_TELEMETRY` - Options `true`|`false`. Default `false`

Enjoy!

## Credits:

[ABIOSOFT](https://github.com/abiosoft/caddy-docker)
